import { createRouter, createWebHistory } from "vue-router"
// const routerHistory = createWebHistory()

const Index = ()=> import('./views/Index.vue')
const Home = ()=> import('./views/Home.vue')
const Components = ()=> import('./views/Components.vue')
const Switch = ()=> import('./components/SwitchDemo.vue')
const Button = ()=> import('./components/ButtonDemo.vue')


const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            redirect: '/index'
        },
        {
            path: '/index',
            component: Index,
        },
        {
            path: '/home',
            component: Home,
        },
        {
            path: '/components',
            component: Components,
            children: [
                {
                    path: 'switch',
                    component: Switch
                },
                {
                    path: 'button',
                    component: Button
                }
            ]
        }
    ]
})

export default  router